-- initialise la table map
-- heure 		 = 1
-- (x,y) 		 = 0,0
-- (spanX,spanY) = 100,100

TRUNCATE TABLE map;

INSERT INTO map (ma_heure, ma_span_lat, ma_span_lon, ma_center_lat, ma_center_lon )
    VALUES(0,  100.0, 100.0, 0.0, 0.0 );

SELECT * FROM map;
