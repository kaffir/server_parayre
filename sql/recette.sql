
--vide les recettes
TRUNCATE TABLE recette;
--vide la composition des recettes par des ingredients
TRUNCATE TABLE constitue;

-- creation d'une nouvelle recette
INSERT INTO recette(re_nom,re_jour_debloque, re_jo_nom)
    VALUES('limonade',1,'Parayre');

--recupere les id des ingredients en fonction de leur nom
SELECT in_id FROM ingredient
WHERE ingredient.in_nom = 'sucre';

--recupere l'id d'une recette en fonction de son nom
SELECT re_id FROM recette
WHERE recette.re_nom = 'limonade';

-- definie une liste d'ingredients pour une recette
INSERT INTO constitue (co_re_id, co_in_id)
    VALUES(1,1),
    (1,2),
    (1,3),
    (1,7);

SELECT in_nom FROM ingredient
INNER JOIN constitue ON constitue.co_in_id = ingredient.in_id
INNER JOIN recette ON constitue.co_re_id = recette.re_id
WHERE recette.re_nom = 'limonade'
;

-- SELECT * FROM ingredient;

