
DROP TABLE IF EXISTS joueur CASCADE;
DROP TABLE IF EXISTS ad CASCADE;
DROP TABLE IF EXISTS ingredient CASCADE;
DROP TABLE IF EXISTS recette CASCADE;
DROP TABLE IF EXISTS day CASCADE;
DROP TABLE IF EXISTS achat CASCADE;
DROP TABLE IF EXISTS production CASCADE;
DROP TABLE IF EXISTS constitue CASCADE;
DROP TABLE IF EXISTS map CASCADE;


CREATE TABLE joueur(
        jo_nom     Varchar (255) NOT NULL ,
        jo_budget  Float ,
        jo_stand_x Float ,
        jo_stand_y Float ,
        jo_actif   Bool ,
        CONSTRAINT PK_jo_nom PRIMARY KEY (jo_nom)
);


CREATE TABLE ad(
        ad_id        SERIAL NOT NULL ,
        ad_influence Float ,
        ad_lat       Float ,
        ad_lon       Float ,
        ad_jo_nom       Varchar (255) ,
        ad_da_id        Int ,
        CONSTRAINT PK_ad_id PRIMARY KEY (ad_id)
);

CREATE TABLE ingredient(
        in_id          SERIAL NOT NULL PRIMARY KEY,
        in_nom         Varchar (255) ,
        in_prix_achat  Float ,
        in_has_alcohol Bool ,
        in_froid       Bool --,
--        CONSTRAINT PK_in_id PRIMARY KEY (in_id)
);


CREATE TABLE recette(
        re_id            SERIAL NOT NULL ,
        re_nom           Varchar (25) ,
        re_jour_debloque Int ,
        re_jo_nom           Varchar (255) ,
        CONSTRAINT PK_re_id PRIMARY KEY (re_id)
);


CREATE TABLE day(
        da_id              SERIAL NOT NULL ,
        da_jour            Int ,
        da_heure           Int ,
        da_meteo_constat   Varchar (255) ,
        da_meteo_prevision Varchar (255) ,
        CONSTRAINT PK_da_id PRIMARY KEY (da_id)
);


CREATE TABLE achat(
        ac_id       SERIAL NOT NULL ,
        ac_jour     Int ,
        ac_heure    Int ,
        ac_prix     Float ,
        ac_effectue Bool ,
        ac_jo_nom      Varchar (255) ,
        ac_re_id       Int ,
        ac_da_id       Int ,
        CONSTRAINT PK_ac_id PRIMARY KEY (ac_id)
);


CREATE TABLE production(
        pr_id         SERIAL NOT NULL ,
        pr_qte        Int ,
        pr_prix_vente Float ,
        pr_jo_nom        Varchar (255) ,
        pr_re_id         Int ,
        pr_da_id         Int ,

        CONSTRAINT PK_pr_id PRIMARY KEY (pr_id)
);


CREATE TABLE map(
        ma_id    SERIAL NOT NULL ,
        ma_heure    Int  NOT NULL,
        ma_span_lat   Float ,
        ma_span_lon   Float ,
        ma_center_lat Float ,
        ma_center_lon Float ,
        CONSTRAINT PK_ma_id PRIMARY KEY (ma_id)
);


CREATE TABLE constitue(
        co_in_id Int  NOT NULL ,
        co_re_id  Int NOT NULL ,
        CONSTRAINT PK_co_in_nom PRIMARY KEY (co_in_id, co_re_id)
);



ALTER TABLE ad ADD CONSTRAINT FK_ad_jo_nom FOREIGN KEY (ad_jo_nom) REFERENCES joueur(jo_nom);
ALTER TABLE ad ADD CONSTRAINT FK_ad_da_id FOREIGN KEY (ad_da_id) REFERENCES day(da_id);
ALTER TABLE recette ADD CONSTRAINT FK_recette_jo_nom FOREIGN KEY (re_jo_nom) REFERENCES joueur(jo_nom);
ALTER TABLE achat ADD CONSTRAINT FK_achat_jo_nom FOREIGN KEY (ac_jo_nom) REFERENCES joueur(jo_nom);
ALTER TABLE achat ADD CONSTRAINT FK_achat_re_id FOREIGN KEY (ac_re_id) REFERENCES recette(re_id);
ALTER TABLE achat ADD CONSTRAINT FK_achat_da_id FOREIGN KEY (ac_da_id) REFERENCES day(da_id);
ALTER TABLE production ADD CONSTRAINT FK_production_jo_nom FOREIGN KEY (pr_jo_nom) REFERENCES joueur(jo_nom);
ALTER TABLE production ADD CONSTRAINT FK_production_re_id FOREIGN KEY (pr_re_id) REFERENCES recette(re_id);
ALTER TABLE production ADD CONSTRAINT FK_production_da_id FOREIGN KEY (pr_da_id) REFERENCES day(da_id);
ALTER TABLE constitue ADD CONSTRAINT FK_constitue_in_id FOREIGN KEY (co_in_id) REFERENCES ingredient(in_id);
ALTER TABLE constitue ADD CONSTRAINT FK_constitue_re_id FOREIGN KEY (co_re_id) REFERENCES recette(re_id);


