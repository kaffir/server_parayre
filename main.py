from flask import Flask, request, json, jsonify, make_response, Response
from flask_cors import CORS
from db import Db
import os, psycopg2, urlparse
import random

# -- Ajout sinon bug .Melanie
import sys
reload(sys)
sys.setdefaultencoding("latin-1")

app = Flask(__name__)
app.debug = True
CORS(app)

# info de connection a la DB:
# postgres://vvbmqcjgrutmbc:7eed8fa77665a4b49cd7252f27fecae5d091e66fe45f58c5d0e56b76ac6ba32f@ec2-107-22-250-33.compute-1.amazonaws.com:5432/dbdfgu8097c10

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: formate des donnees en JSON pour les return
def jsonResponse(data, status=200):
  return json.dumps(data), status, {'Content-Type': 'application/json'}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: retourne le nombre d'heure totales ecoulees dans le jeu [0...N]
def getHour():
  database = Db()
  hour = database.select("SELECT ma_heure FROM map")
  database.close()
  return hour[0]["ma_heure"]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: retourne l'heure de la journee [0...23]
def getHourDay():
  return getHour()%24

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: retourne le 'numero' de la journee [0...N]
def getDay():
  return ((getHour()-getHourDay())/24)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de savoir si un joueur est actif / connecte [true/false]
def isPlayerActif(name):
  database = Db()
  actif = database.select("SELECT jo_actif FROM joueur WHERE joueur.jo_name = @(nom)",{'nom':name})
  database.close()
  return actif[0]["jo_actif"]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: retourne l'id du dernier jour (dans map) [0...N]
def getMapId():
  database = Db()
  mapId = database.select("SELECT ma_id FROM map ORDER BY ma_id DESC")
  database.close()
  return mapId[0]["ma_id"]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de connaitre l'id d'une recette identifiee [0...N]
# param: recette = le nom de la recette 
#        player  = le nom du proprietaire 
def getRecetteId(recette, player):
  database = Db()
  recetteId = database.select("SELECT re_id FROM recette WHERE re_nom = @(recette) AND re_jo_nom = @(player)",{
    'recette': recette,
    'player' : player
  })
  database.close()
  return recetteId[0]["re_id"]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de connaitre le cout de fabrication d'une recette [0...N]
# param: recette = le nom de la recette 
#        player  = le nom du proprietaire 
def getRecettePrix(recette, player):
  dataBase = Db()
  # avoir l'id du nom de la recette (recette)
  recetteId = getRecetteId(recette, player)
  # recupere dans constitue les id des ingredients
  ingredientsId = dataBase.select('SELECT co_in_id FROM constitue WHERE co_re_id = @(id_recette)', {
    'id_recette' : recetteId
  })
  prixRecette = 0.0
  i = 0
  # pour tous les ingredients qui compose la recette:
  for i in range(0, len(ingredientsId)):
    ingredientPrix = dataBase.select("SELECT in_prix_achat FROM ingredient WHERE ingredient.in_id = @(id)", {
      'id' : ingredientsId[i]["co_in_id"]
    })
    prixRecette += ingredientPrix[0]['in_prix_achat']
  dataBase.close()
  return round(prixRecette,2)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de savoir si une recette est Froide [true/false]
#         on considere que le froid prend le dessus sur le chaud
# param: recette = le nom de la recette 
#        player  = le nom du proprietaire 
def getRecetteIsCold(recette, player):
  dataBase = Db()
  # avoir l'id du nom de la recette (recette)
  recetteId = getRecetteId(recette, player)
  # recupere dans constitue les id des ingredients
  ingredientsId = dataBase.select('SELECT co_in_id FROM constitue WHERE co_re_id = @(id_recette)', {
    'id_recette' : recetteId
  })
  i = 0
  # pour tous les ingredients :
  # Si au moins un seul ingredient est froid on considere la recette comme froide
  for i in range(0, len(ingredientsId)):
    ingredientPrix = dataBase.select("SELECT in_froid FROM ingredient WHERE ingredient.in_id = @(id)", {
      'id' : ingredientsId[i]["co_in_id"]
    })
    if ingredientPrix[0]['in_froid'] == True:
      dataBase.close()
      return True
  dataBase.close()
  return False

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de savoir si une recette est alcoolisee [true/false]
#         on considere que l'alcool prend le dessus sur le non-alcoolisee
# param: recette = le nom de la recette 
#        player  = le nom du proprietaire 
def getRecetteHasAlcohol(recette, player):
  dataBase = Db()
  # avoir l'id d'une recette
  recetteId = getRecetteId(recette, player)
  # recupere dans constitue tous les ids des ingredients qui composent une recette
  ingredientsId = dataBase.select('SELECT co_in_id FROM constitue WHERE co_re_id = @(id_recette)', {
    'id_recette' : recetteId
  })
  i = 0
  # pour tous les ingredients :
  # Si au moins un seul ingredient est alcolise on considere la recette comme alcolise
  for i in range(0, len(ingredientsId)):
    ingredientPrix = dataBase.select("SELECT in_has_alcohol FROM ingredient WHERE ingredient.in_id = @(id)", {
      'id' : ingredientsId[i]["co_in_id"]
    })
    if ingredientPrix[0]['in_has_alcohol'] == True:
      dataBase.close()
      return True
  dataBase.close()
  return False
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# resume: permet de changer l'etat d'activite d'un joueur
# param: etat = etat d'activite d'un joueur [true/false]
#        player  = le nom du proprietaire ['string']
def setPlayerEtat(player, etat):
  dataBase = Db()
  dataBase.execute("UPDATE joueur SET jo_actif = @(etat) WHERE jo_nom = (@(jo_nom))",{
    'etat'  : etat,
    'jo_nom': nom
  })
  dataBase.close()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/metrology", methods=["POST"])
# resume: permet de mettre a jour l'heure, la date et les infos de meteo [OK]
# JSON attendu : '{"timestamp":57 ,"weather":[{"dfn":0, "weather":"sunny"}, {"dfn":1, "weather":"rainy"}]}'
def PostMetrology():
  retour = json.loads(request.get_data()) #recuperation de la requete JSON
  #  return json.dumps(retour)
  dataBase = Db() #Insertion d'une nouvelle entree dans la table "day"
  dataBase.execute("INSERT INTO day (da_jour, da_heure , da_meteo_constat, da_meteo_prevision) VALUES(@(da_jour),@(da_heure),@(da_meteo_constat),@(da_meteo_prevision))",{
    'da_jour'  : retour["timestamp"]/24, #jour actuel
    'da_heure' : retour["timestamp"]%24,
    'da_meteo_constat' : retour["weather"][0]["weather"],
    'da_meteo_prevision': retour["weather"][1]["weather"]
  })
  # Maj de l'heure dans map
  dataBase.execute("UPDATE  map SET ma_heure = @(heure)",{
    'heure' : retour["timestamp"]
  })
  dataBase.close()
  return "OK"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/metrology", methods=["GET"])
# resume: permet de savoir les infos d'un jour [JSON]
# JSON retourne : '{"timestamp":57 ,"weather":[{"dfn":0, "weather":"sunny"}, {"dfn":1, "weather":"rainy"}]}'
def GetMetrology():
  dataBase = Db()
  # recuperation des infos pout le jour actuel
  retour = dataBase.select("SELECT da_id, da_jour, da_heure , da_meteo_constat, da_meteo_prevision FROM day WHERE da_heure = @(heure) AND da_jour = @(jour)",{
    'jour'  : getDay(),
    'heure' : getHourDay() 
  })
  dataBase.close()
  # preparation format JSON 
  nbHeure     = retour[0]['da_jour']*24 + retour[0]['da_heure']
  constat     = retour[0]['da_meteo_constat']
  prevision   = retour[0]['da_meteo_prevision']
  json_retour = {"timestamp":nbHeure,"weather":[{"dfn":0,"weather":constat},{"dfn":1,"weather":prevision}]}
  return jsonResponse(json_retour)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/players/<nom>", methods=["DELETE"])
def playerDelete(nom):
  data = request.json
  dataBase = Db()
  nomJoueur = dataBase.select("SELECT jo_nom FROM joueur WHERE jo_nom = @(nom) ",{
    'nom' : nom
  })
  if nomJoueur: # si le joueur existe
    setPlayerEtat(nom, False)
    retour = {"suppression":"OK"}
  else:
    retour = {"suppression":"KO"}
  dataBase.close()
  return jsonResponse(retour)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/sales", methods=["POST"])
# resume: permet de tenter d'effectuer une vente par la JAVA [JSON]
# Exemple POST : curl -H "Content-Type: application/json" -X POST -d '{"sales":[{"player":"Parayre", "item":"limonade","quantity":0}]}' http://127.0.0.1:5000/sales

def sales():
  status = True
  newSales = json.loads(request.get_data())
  database = Db()
  day  = getDay()     # valeur entre [0... N]
  hour = getHourDay() # valeur entre [0...23]
 
  for sale in newSales["sales"]:
    produced = database.select("SELECT pr_qte, pr_prix_vente, recette.re_id FROM production, joueur, recette, day WHERE recette.re_nom = @(drink) AND joueur.jo_nom = @(player) AND day.da_jour = @(day) AND day.da_id = production.pr_da_id AND joueur.jo_nom = production.pr_jo_nom AND recette.re_id = production.pr_re_id ", {
      'drink' : sale["item"],
      'player' : sale["player"],
      'day' : day-1
    })
    if len(produced) == 0:
      produced.append(0)
      produced.append(0)
      recipe = database.select("SELECT recette.re_id FROM recette INNER JOIN joueur ON joueur.jo_nom = recette.re_jo_nom WHERE recette.re_nom = @(drink) AND joueur.jo_nom = @(player)", {
        'drink' : sale["item"],
        'player' : sale["player"],
      })
      if len(recipe) == 0:
        recipe.append(None)
      produced.append(recipe[0])
    sold = database.select("SELECT SUM(ac_qte) FROM achat INNER JOIN day ON day.da_id = achat.ac_da_id INNER JOIN joueur ON joueur.jo_nom = achat.ac_jo_nom INNER JOIN recette ON recette.re_id = achat.ac_re_id WHERE recette.re_nom = @(drink) AND joueur.jo_nom = @(player) AND day.da_jour = @(day) AND achat.ac_effectue = @(effectue);", {
      'drink' : sale["item"],
      'player' : sale["player"],
      'day' : day,
      'effectue': True
    })
    if sold[0]["sum"] == None:
      sold[0]["sum"] = 0
    dayID = database.select("SELECT MAX(da_id) FROM day;")
    if(produced[0]-sold[0]["sum"] >= sale["quantity"]):
      database.execute("INSERT INTO achat (ac_jour, ac_heure , ac_qte, ac_prix, ac_effectue, ac_jo_nom, ac_re_id, ac_da_id) VALUES (@(jour),@(heure),@(qte),@(prix),@(effectue),@(player),@(recipe),@(day))", {
        'jour' : day,
        'heure' : hour,
        'qte' : sale["quantity"],
        'prix': produced[1],
        'effectue': True,
        'player': sale["player"],
        'recipe': produced[2]["re_id"],
        'day': dayID[0]["max"]
      })
      budget = database.select("SELECT jo_budget FROM joueur WHERE joueur.jo_nom = @(player);", {
        'player' : sale["player"]
      })
      database.execute("UPDATE joueur SET jo_budget = @(budget) WHERE jo_nom = @(player);", {
        'budget' : budget[0]["jo_budget"]+sale["quantity"]*produced[1],
        'player' : sale["player"]
      })
    else:
      database.execute("INSERT INTO achat (ac_jour, ac_heure , ac_qte, ac_prix, ac_effectue, ac_jo_nom, ac_re_id, ac_da_id) VALUES(@(jour),@(heure),@(qte),@(prix),@(effectue),@(player),@(recipe),@(day))", {
        'jour' : day,
        'heure' : hour,
        'qte' : sale["quantity"],
        'prix' : produced[1],
        'effectue' : False,
        'player': sale["player"],
        'recipe': produced[2]["re_id"],
        'day': dayID[0]["max"]
      })
      status = False
#  retour = { "sales": [ { "player":"Jean-pierre", "item":"biere", "quantity":10 }, { "player":"Polnareff", "item":"limonade", "quantity":105 } ] }
    response = { "response": status }
    database.close()
    return jsonResponse(response)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/map", methods=["GET"])
def getMap():
	database = Db()
	mapInfo = database.select("SELECT ma_center_X, ma_center_Y, ma_span_X, ma_span_Y FROM map WHERE ma_heure = (SELECT MAX(ma_heure) FROM map)")
	region = {"center":{ "latitude": mapInfo[0]["ma_center_x"], "longitude": mapInfo[0]["ma_center_y"]},"span":{ "latitudeSpan": mapInfo[0]["ma_span_x"], "longitudeSpan": mapInfo[0]["ma_span_y"] }}
	ranking = []
	playersByRank = database.select("SELECT jo_nom FROM joueur ORDER BY jo_budget DESC")
	i=0
	while i<len(playersByRank):
		ranking.append(playersByRank[i]["jo_nom"])
		i = i+1

	itemsByPlayer = {}
	drinksByPlayer = {}
	playerInfo = {}

	for playerName in ranking:
		drinksOffered = []
		playerBudget = database.select("SELECT jo_budget FROM joueur WHERE joueur.jo_nom = @(player)", { 
			'player' : playerName
		})
		playerSales = database.select("SELECT SUM(ac_qte) FROM joueur, achat WHERE joueur.jo_nom = @(player) AND achat.ac_jo_nom = joueur.jo_nom", { 
			'player' : playerName
		})
		playerProfit = database.select("SELECT SUM(ac_qte*ac_prix) FROM joueur, achat WHERE joueur.jo_nom = @(player) AND achat.ac_jo_nom = joueur.jo_nom", { 
			'player' : playerName
		})
		playerDrinks = database.select("SELECT re_nom, pr_prix_vente FROM joueur, production, recette, ingredient WHERE joueur.jo_nom = @(player) AND recette.re_id = production.pr_re_id AND joueur.jo_nom = production.pr_jo_nom", { 
			'player' : playerName
		})
		for drinks in playerDrinks:
			hasAlcohol = getRecetteHasAlcohol(drinks["re_nom"], playerName)
			isCold = getRecetteIsCold(drinks["re_nom"], playerName)
			drinksOffered.append({"name":drinks["re_nom"], "price":drinks["pr_prix_vente"], "hasAlcohol":hasAlcohol, "isCold":isCold})
		player = {"cash": playerBudget[0]["jo_budget"], "sales": playerSales[0]["sum"], "profit": playerProfit[0]["sum"], "drinksOffered": drinksOffered}
		playerInfo[playerName] = player

	database.close()
	database = Db()
	for playerName in ranking:
		drink = []
		playerDrinks = database.select("SELECT re_nom FROM recette WHERE recette.re_jo_nom = @(player)", { 
			'player' : playerName
		})
		for drinks in playerDrinks:
			hasAlcohol = getRecetteHasAlcohol(drinks["re_nom"], playerName)
			isCold = getRecetteIsCold(drinks["re_nom"], playerName)
			drink.append({"name":drinks["re_nom"], "price":0, "hasAlcohol":hasAlcohol, "isCold":isCold})
		drinksByPlayer[playerName] = drink

	database.close()
	database = Db()
	
	for playerName in ranking:
		item = []
		stand = database.select("SELECT jo_stand_x, jo_stand_y FROM joueur WHERE joueur.jo_nom = @(player)", { 
			'player' : playerName
		})
		item.append({"kind":"stand", "owner":playerName, "location":{"latitude":stand[0]["jo_stand_x"], "longitude":stand[0]["jo_stand_y"]}, "influence":10})
		ads = database.select("SELECT ad_x, ad_y, ad_influence FROM ad INNER JOIN day ON ad.ad_da_id = day.da_id WHERE ad.ad_jo_nom = @(player) AND day.da_jour = @(day)", { 
			'player' : playerName,
			'day' : getDay()-1
		})
		for ad in ads:
			item.append({"kind":"ad", "owner":playerName, "location":{"latitude":ad[0]["ad_x"], "longitude":ad[0]["ad_y"]}, "influence":ad[0]["ad_influence"]})
		itemsByPlayer[playerName] = item

	database.close()
	response = {"map": {"region":region, "ranking":ranking, "playerInfo":playerInfo, "drinksByPlayer":drinksByPlayer, "itemsByPlayer":itemsByPlayer}}
	return jsonResponse(response)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/map/<string:name>", methods=["GET"])
def getMapPlayer(name):
	database = Db()
	mapInfo = database.select("SELECT ma_center_X, ma_center_Y, ma_span_X, ma_span_Y FROM map WHERE ma_heure = (SELECT MAX(ma_heure) FROM map)")
	region = {"center":{ "latitude": mapInfo[0]["ma_center_x"], "longitude": mapInfo[0]["ma_center_y"]},"span":{ "latitudeSpan": mapInfo[0]["ma_span_x"], "longitudeSpan": mapInfo[0]["ma_span_y"] }}
	ranking = []
	playersByRank = database.select("SELECT jo_nom FROM joueur ORDER BY jo_budget DESC")
	i=0
	while i<len(playersByRank):
		ranking.append(playersByRank[i]["jo_nom"])
		i = i+1

	itemsByPlayer = {}
	drinksByPlayer = {}
	availableIngredients = []

	listeIngredients = database.select("SELECT in_nom, in_prix_achat, in_has_alcohol, in_froid FROM ingredient")
	for ing in listeIngredients:
		availableIngredients.append({"name":ing["in_nom"],"cost":ing["in_prix_achat"],"hasAlcohol":ing["in_has_alcohol"],"isCold":ing["in_froid"]})
	
	drinksOffered = []
	playerBudget = database.select("SELECT jo_budget FROM joueur WHERE joueur.jo_nom = @(player)", { 
		'player' : name
	})
	playerSales = database.select("SELECT SUM(ac_qte) FROM joueur, achat WHERE joueur.jo_nom = @(player) AND achat.ac_jo_nom = joueur.jo_nom", { 
		'player' : name
	})
	playerProfit = database.select("SELECT SUM(ac_qte*ac_prix) FROM joueur, achat WHERE joueur.jo_nom = @(player) AND achat.ac_jo_nom = joueur.jo_nom", { 
		'player' : name
	})
	playerDrinks = database.select("SELECT re_nom, pr_prix_vente FROM joueur, production, recette, ingredient WHERE joueur.jo_nom = @(player) AND recette.re_id = production.pr_re_id AND joueur.jo_nom = production.pr_jo_nom", { 
		'player' : name
	})
	for drinks in playerDrinks:
		hasAlcohol = getRecetteHasAlcohol(drinks["re_nom"], name)
		isCold = getRecetteIsCold(drinks["re_nom"], name)
		drinksOffered.append({"name":drinks["re_nom"], "price":drinks["pr_prix_vente"], "hasAlcohol":hasAlcohol, "isCold":isCold})
	playerInfo = {"cash": playerBudget[0]["jo_budget"], "sales": playerSales[0]["sum"], "profit": playerProfit[0]["sum"], "drinksOffered": drinksOffered}

	database.close()
	database = Db()
	
	for playerName in ranking:
		item = []
		stand = database.select("SELECT jo_stand_x, jo_stand_y FROM joueur WHERE joueur.jo_nom = @(player)", { 
			'player' : playerName
		})
		item.append({"kind":"stand", "owner":playerName, "location":{"latitude":stand[0]["jo_stand_x"], "longitude":stand[0]["jo_stand_y"]}, "influence":10})
		ads = database.select("SELECT ad_x, ad_y, ad_influence FROM ad INNER JOIN day ON ad.ad_da_id = day.da_id WHERE ad.ad_jo_nom = @(player) AND day.da_jour = @(day)", { 
			'player' : playerName,
			'day' : getDay()-1
		})
		for ad in ads:
			item.append({"kind":"ad", "owner":playerName, "location":{"latitude":ad[0]["ad_x"], "longitude":ad[0]["ad_y"]}, "influence":ad[0]["ad_influence"]})
		itemsByPlayer[playerName] = item

	database.close()
	response = {"availableIngredients":availableIngredients, "map": {"region":region, "ranking":ranking, "itemsByPlayer":itemsByPlayer}, "playerInfo":playerInfo}
	return jsonResponse(response)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@app.route("/players", methods=["POST"])
def players():

  data = request.json
  dataBase = Db()
  nom = str(data['name']) # lit le nom envoye en parametre


  findJoueur = dataBase.select("SELECT jo_nom FROM joueur WHERE jo_nom LIKE @(nom) ",{
    'nom' : nom
  })

  if not findJoueur: # si le joueur n'existe pas
  # ajout d'un joueur dans la DB
    dataBase.execute("INSERT INTO joueur (jo_nom , jo_budget, jo_stand_x, jo_stand_y, jo_actif) VALUES(@(jo_nom),@(jo_budget),@(jo_stand_x),@(jo_stand_y),@(jo_actif))",{
	  'jo_nom'    : nom,
	  'jo_budget' : 2,
	  'jo_stand_x': random.uniform(0.0,100.0),
	  'jo_stand_y': random.uniform(0.0,100.0),
	  'jo_actif'  : True
	})

  # infos sur le joueur en fonction du nom
  infoJoueur = dataBase.select("SELECT * FROM joueur WHERE jo_nom LIKE @(nom) ",{
    'nom' : nom
  })

  dataBase.execute("UPDATE joueur SET jo_actif = true WHERE jo_nom = (@(jo_nom))",{
    'jo_nom'  : nom
  })
  dataBase.close()

# prepare objet cash/sales
  info = {
    'cash' : infoJoueur[0]['jo_budget'],
    'profit' : 50.0,
    'sales' : 0 # TODO a changer avec un autre SELECT
  }

# prepare objet position
  location = {
    'latitude' : infoJoueur[0]['jo_stand_x'],
    'longitude' : infoJoueur[0]['jo_stand_y']
  }

# prepare objet principal
  data = {
    'name' : infoJoueur[0]['jo_nom'],
    'location' : location,
    'info' : info
  }

  return jsonResponse(data)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/action/<name>", methods=["POST"])
def action(name):

  # initialisation
  data = json.loads(request.get_data())
  jsonData = data['action']
  dataBase = Db()
  phase = 0
  coutTotal = 0.0
  # lit le JSON pour tous les ['kind']
  phase = 0
  for phase in range(0, 1):
    for action in jsonData:
      # traitement nouvelle recette
      if action['kind'] == 'recipe' : # ancienne version
        # recupere l'id d'une recette en fonction de son nom
        if phase == 0:
          coutTotal += 10
        elif phase == 1:
          id_recette = dataBase.select("SELECT re_id FROM recette WHERE recette.re_nom = @(recette) AND recette.re_jo_nom = @(jo_nom) ", {
            'recette' : action['recipe']['name'], #Exemple: limonade
            'jo_nom'  : name
          })
          return jsonResponse(id_recette)
          # -- recupere les ids des ingredients
          for ingredients in action['recipe']['ingredient']:
            id_ingredient = dataBase.select("SELECT in_id FROM ingredient WHERE ingredient.in_nom = @(ingr)", {
              'ingr' : ingredients['name'] #= citron
            })
            # verifier que la recette n'existe pas deja:
            # TODO
            # action['recipe']['name'] #= limonade
            dataBase.execute('INSERT INTO recette(re_nom, re_jour_debloque, re_jo_nom) VALUES (@(name), @(jour), @(jo_nom))', {
              'name'   : action['recipe']['name'],
              'jour'   : getDay(),
              'jo_nom' : name
            })
            # definie une liste d'ingredients pour une recette
            dataBase.execute('INSERT INTO constitue(co_re_id, co_in_id) VALUES (@(id_recette), @(id_ingr))', {
              'id_recette'     : id_recette[0]['re_id'],
              'id_ingr' : id_ingredient[0]['in_id']
            })
      # fin - traitement nouvelle recette
      elif action['kind'] == 'ad':# traitement ajout d'une pub
        if phase == 0:
          coutTotal += action['influence']
        elif phase == 1:
          latitude  = action['location']['latitude']  #= 54.8
          longitude = action['location']['longitude'] #= 24.9
          influence = action['influence'] #= 2

          id_day = dataBase.select("SELECT da_id from day ORDER BY da_id DESC")
          # insere la publicite pour le joueur, pour le jour actuel
          dataBase.execute('INSERT INTO ad (ad_influence, ad_x, ad_y, ad_jo_nom, ad_da_id) VALUES (@(influence), @(lat), @(lon), @(jo_nom), @(ad_da_id))', {
            'influence' : influence,
            'lon'       : longitude,
            'lat'       : latitude,
            'jo_nom'    : name,
            'ad_da_id'  : id_day[0]['da_id']
          })
      # fin - traitement nouvelle Ad 
      elif action['kind'] == 'drinks':# traitement creation de boissons pour j+1
        if phase == 0:
          for key, value in (action['prepare'].items()):# key = 'limonade' OU 'cafe'
            #return str(key) + "-" + str(value)
            prixFabrication = action['prepare'][key]*getRecettePrix(key, name)
            coutTotal += prixFabrication
        elif phase == 1:
          i = 0
          for key, value in (action['prepare'].items()):# key = 'limonade' OU 'cafe'
            id_day = dataBase.select("SELECT da_id from day ORDER BY da_id DESC")
            id_recette = getRecetteId(key, name)
            dataBase.execute('INSERT INTO production (pr_qte, pr_prix_vente, pr_jo_nom, pr_re_id, pr_da_id ) VALUES (@(qt), @(prix), @(jo), @(re), @(da))', {
              'qt'      : action['prepare'][key],
              'prix'    : action['price'][key],
              'jo'     : name,
              're'  : id_recette[0]['re_id'],#key = 'limonade', je veux l'index
              'da' : id_day[0]['da_id']
            })
  dataBase.close()
  return jsonResponse({"success": True, "cost": coutTotal})

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/reset", methods=["GET"])
def reset():
  dataBase = Db()  
  dataBase.executeFile("sql/reset.sql")
  dataBase.close()
  # retourne du Json pour confirmer le reset
  retour = {"reset": "ok"}
  return jsonResponse(retour)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/ingredients", methods=["GET"])
def ingredients():
  retour = {}

  ingredient = []
  dataBase = Db()  
  info = dataBase.select("SELECT * FROM ingredient")
  dataBase.close()

  i=0
  # remplit le Json pour
  while i < len(info):
    data = {
     'name'      : info[i]['in_nom'],
     'cost'      : info[i]['in_prix_achat'],
     "hasAlcohol": info[i]['in_has_alcohol'],
     "isCold"    : info[i]['in_froid']
    }
    ingredient.append(data)
    i = i+1
  return json.dumps({'ingredient': ingredient}), 200, {'Content-Type': 'application/json'}


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == "__main__":
	app.run()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/budget/<player>", methods=["GET"])
def budget(player):
  retour = {}
  retour = { "jour": 5, "heure": 12, "budget": 50.45, "depenseTotal": 10.55, "venteTotal": 60 }
  return jsonResponse(retour)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.route("/recettes/<player>", methods=["GET"])
def recettes(player):
  retour = {}
  retour = { "boisson": [ { "nom": "limonade", "prix": 0.5, "ingredients":[ "sucre","eau","citron","glacon"]}, { "nom": "mojito", "prix": 1.0, "ingredients":["menthe","eau gazeuse","citron","glacon","rhum"]}, { "nom": "cafe", "prix": 0.8, "ingredients":["sucre","eau","cafe"]} ] }
  return jsonResponse(retour)


